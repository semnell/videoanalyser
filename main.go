/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitlab.com/semnell/videoanalyser/cmd"

func main() {
	cmd.Execute()
}
