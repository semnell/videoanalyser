/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/spf13/cobra"
)

var no_check bool
var exists bool
var title string

// addCmd represents the add command
var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Add a new video to the database",
	Long:  `Add a new video to the database. Example: videoanalyser add https://www.youtube.com/watch?v=1234567890 `,
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("add called")
		db := setup()
		url := args[0]
		fmt.Println("Adding video", url)
		// check if the video is already in the database
		var video Video
		re := db.Where("ref = ?", url)
		re.Find(&video)
		if no_check {
			video.ID = 0
		}
		if video.ID != 0 {
			fmt.Println("Video already in the database")
			return
		} else {
			if !no_check {
				exists, title = CheckIfVideoExists(url)
			} else {
				exists = true
				title = "No title"
			}
			var ref string
			if exists {
				regexp := regexp.MustCompile(`^https?:\/\/[A-Za-z0-9:.]*$`)
				ref = regexp.FindString(ref)
				if ref == "" {
					ref = ""
				} else {
					res := regexp.FindAllStringSubmatch(ref, -1)
					ref = res[0][0]
				}

				// remove trailing slash
				ref = strings.TrimSuffix(url, "/")
			}
			// add video to database
			if exists {
				// get current date
				date := time.Now().Format("2006-01-02")
				db.Create(&Video{Url: args[0], Name: title, Available: exists, Ref: ref, DateAdded: date})
			} else if !exists {
				fmt.Println("Video not available, it did not return a successful status code")
			} else {
				fmt.Println("Video not available, this does not look like a valid video url")
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(addCmd)
	// add no check flag
	addCmd.Flags().BoolVarP(&no_check, "no-check", "n", false, "Do not check if the video is available")
}
