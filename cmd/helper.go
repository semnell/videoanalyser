package cmd

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/manifoldco/promptui"
)

func CheckIfVideoExists(url string) (bool, string) {
	if strings.HasPrefix(url, "https://www.youtube.com/watch?v=") {
		// check if video exists
		resp, err := http.Get(url)
		if err != nil {
			return false, ""
		}
		// test if redirect
		if resp.StatusCode == 302 {
			return false, ""
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			return false, ""
		}
		// find "playabilityStatus":{"status":"ERROR" in the html
		doc, err := goquery.NewDocument(url)
		if err != nil {
			return false, ""
		}
		playabilityStatus := doc.Find("script").Text()
		if strings.Contains(playabilityStatus, `"playabilityStatus":{"status":"ERROR"`) {
			return false, ""
		}
		// get title
		if err != nil {
			return false, ""
		}
		title := doc.Find("meta[name=title]").AttrOr("content", "")
		if title == "" {
			var cont string
			fmt.Println("title is empty or unable to detect, do you want add one yourself? (y/n)")
			fmt.Scanln(&cont)
			if cont == "y" {
				prompt := promptui.Prompt{
					Label: "Title",
				}
				result, err := prompt.Run()
				if err != nil {
					fmt.Printf("Prompt failed %v\n", err)
					os.Exit(1)
				}
				return true, result
			} else {
				return true, ""
			}
		}
		return true, title
	} else if strings.HasPrefix(url, "https://vimeo.com/") {
		// check if video exists
		resp, err := http.Get(url)
		if err != nil {
			return false, ""
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			return false, ""
		}
		// get title
		doc, err := goquery.NewDocument(url)
		if err != nil {
			return false, ""
		}

		title := doc.Find("head > meta:nth-child(13)").AttrOr("content", "")
		if title == "" {
			var cont string
			fmt.Println("title is empty or unable to detect, do you want add one yourself? (y/n)")
			fmt.Scanln(&cont)
			if cont == "y" {
				prompt := promptui.Prompt{
					Label: "Title",
				}
				result, err := prompt.Run()
				if err != nil {
					fmt.Printf("Prompt failed %v\n", err)
					os.Exit(1)
				}
				return true, result
			} else {
				return true, ""
			}
		}
		fmt.Println("found -> ", title)
		if title == "" {
			var cont string
			fmt.Println("title is empty, do you want to continue? (y/n)")
			fmt.Scanln(&cont)
			if cont == "y" {
				return true, ""
			} else {
				return false, ""
			}
		}
		return true, title
	} else if strings.HasPrefix(url, "https://player.vimeo.com/video/") {
		// check if video exists
		resp, err := http.Get(url)
		if err != nil {
			return false, ""
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			return false, ""
		}
		// get title

		doc, err := goquery.NewDocument(url)
		if err != nil {
			return false, ""
		}
		title := doc.Find("head > title").AttrOr("content", "")
		if title == "" {
			var cont string
			fmt.Println("title is empty or unable to detect, do you want add one yourself? (y/n)")
			fmt.Scanln(&cont)
			if cont == "y" {
				prompt := promptui.Prompt{
					Label: "Title",
				}
				result, err := prompt.Run()
				if err != nil {
					fmt.Printf("Prompt failed %v\n", err)
					os.Exit(1)
				}
				return true, result
			} else {
				return true, ""
			}
		}
		return true, title
	} else {
		return false, ""
	}

}

func RunCliCommand(args []string) (out string) {
	old := os.Stdout // keep backup of the real stdout
	r, w, _ := os.Pipe()
	os.Stdout = w
	rootCmd.SetArgs(args)
	err := rootCmd.Execute()
	if err != nil {
		fmt.Println(err)
	}
	outC := make(chan string)
	// copy the output in a separate goroutine so printing can't block indefinitely
	go func() {
		var buf bytes.Buffer
		_, err := io.Copy(&buf, r)
		if err != nil {
			fmt.Println(err)
		}
		outC <- buf.String()
	}()
	w.Close()
	os.Stdout = old // restoring the real stdout
	out = <-outC
	return out
}
