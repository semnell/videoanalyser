/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

var (
	noPrintAvailableVideos   bool
	noPrintUnavailableVideos bool
	noColor                  bool
)

// checkCmd represents the check command
var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "Check all videos in the database",
	Run: func(cmd *cobra.Command, args []string) {
		// check all videos in the database
		db := setup()
		var videos []Video
		db.Find(&videos)
		var availableVideos []Video
		var unavailableVideos []Video
		len_videos := len(videos)
		fmt.Println("Checking", len_videos, "videos")
		videa_iterable := make([]string, len_videos)
		var wg sync.WaitGroup
		wg.Add(len_videos)
		for i, video := range videos {
			go func(i int, video Video) {
				defer wg.Done()
				resp, err := http.Get(video.Url)
				if err != nil {
					fmt.Println(err)
					return
				}
				defer resp.Body.Close()
				exists, _ := CheckIfVideoExists(video.Url)
				if exists {
					availableVideos = append(availableVideos, video)
					videa_iterable[i] = "Available"
				} else {
					unavailableVideos = append(unavailableVideos, video)
					videa_iterable[i] = "Unavailable"
				}
			}(i, video)
		}
		wg.Wait()
		// add all videos to single array
		var allVideos []Video
		allVideos = append(allVideos, availableVideos...)
		allVideos = append(allVideos, unavailableVideos...)
		// print all videos
		var avail []string
		var unavail []string
		for i, video := range allVideos {
			if videa_iterable[i] == "Available" {
				avail = append(avail, video.Url)
				db.Model(&video).Update("available", true)
			} else {
				unavail = append(unavail, video.Url)
				db.Model(&video).Update("available", false)
			}
		}
		if len(avail) > 0 && !noPrintAvailableVideos {

			fmt.Println("Available videos:")
			for _, video := range avail {
				if !noColor {
					color.Green(video)
				} else {
					fmt.Println(video)
				}
			}
		}
		if len(unavail) > 0 && !noPrintUnavailableVideos {
			fmt.Println("All unavailable videos:")
			for _, video := range unavail {
				if !noColor {
					color.Red(video)
				} else {
					fmt.Println(video)
				}
			}
		}

	},
}

func init() {
	rootCmd.AddCommand(checkCmd)
	// add no print available videos flag
	checkCmd.Flags().BoolVarP(&noPrintAvailableVideos, "no-print-avail", "n", false, "Don't print available videos")
	// add no print unavailable videos flag
	checkCmd.Flags().BoolVarP(&noPrintUnavailableVideos, "no-print-unavail", "u", false, "Don't print unavailable videos")
	// add no color flag
	checkCmd.Flags().BoolVarP(&noColor, "no-color", "c", false, "Don't use color")
}
