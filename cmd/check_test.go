package cmd

import (
	"fmt"
	"os"
	"strings"
	"testing"
)

func TestCheckIfVideoCheck(t *testing.T) {
	// make sure database doesnt exist
	if _, err := os.Stat("Videos.db"); err == nil {
		os.Remove("Videos.db")
	}
	out := RunCliCommand([]string{"add", "https://www.youtube.com/watch?v=jNQXAC9IVRw"})
	fmt.Print(out)
	if strings.Contains(out, "Video not available") {
		t.Errorf("This vid should be available")
	}
	RunCliCommand([]string{"add", "https://www.youtube.com/watch?v=1234567", "--no-check"})
	out = RunCliCommand([]string{"check", "--no-color"})
	if !strings.Contains(out, "jNQXAC9IVRw") {
		t.Errorf("This vid should be available")
	}
	if !strings.Contains(out, "1234567") {
		t.Errorf("This vid shouldnt be available")
	}
	if _, err := os.Stat("Videos.db"); err == nil {
		os.Remove("Videos.db")
	}

}
