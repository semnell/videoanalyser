/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all videos in the database",
	Run: func(cmd *cobra.Command, args []string) {
		// list all videos in the database
		db := setup()
		var videos []Video
		db.Find(&videos)
		fmt.Print("title | available | ref | date\n")
		for _, video := range videos {
			if video.Available {
				color.Set(color.FgGreen)
				fmt.Println(video.Name, " | ", video.Available, " | ", video.Ref, " | ", video.DateAdded)
			} else {
				color.Set(color.FgRed)
				fmt.Println(video.Name, " | ", video.Available, " | ", video.Ref, " | ", video.DateAdded)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
}
