package cmd

import (
	"os"
	"strings"
	"testing"
)

func TestDeleteVideo(t *testing.T) {
	if _, err := os.Stat("Videos.db"); err == nil {
		os.Remove("Videos.db")
	}
	RunCliCommand([]string{"add", "https://www.youtube.com/watch?v=jNQXAC9IVRw"})
	RunCliCommand([]string{"add", "https://www.youtube.com/watch?v=1234567", "--no-check"})
	out := RunCliCommand([]string{"delete", "https://www.youtube.com/watch?v=jNQXAC9IVRw"})
	if !strings.Contains(out, "Video deleted") {
		t.Errorf("This vid should be deleted")
	}
	out = RunCliCommand([]string{"check", "--no-color"})
	if strings.Contains(out, "jNQXAC9IVRw") {
		t.Errorf("This vid should be deleted")
	}
	if !strings.Contains(out, "1234567") {
		t.Errorf("This vid shouldnt be deleted")
	}
	out = RunCliCommand([]string{"delete", "https://www.youtube.com/watch?v=jNQXAC9IVRw"})
	if !strings.Contains(out, "Video not found") {
		t.Errorf("This vid shouldnt be deleted")
	}
	if _, err := os.Stat("Videos.db"); err == nil {
		os.Remove("Videos.db")
	}
}
