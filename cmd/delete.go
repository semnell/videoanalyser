/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// deleteCmd represents the delete command
var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a video from the database",
	Long:  `Delete a video from the database. Example: videoanalyser delete https://www.youtube.com/watch?v=1234567890 `,
	Run: func(cmd *cobra.Command, args []string) {
		// delete a video from the database
		db := setup()
		var video Video
		db.Where("ref = ?", args[0]).Find(&video)
		if video.ID != 0 {
			db.Delete(&video)
			fmt.Println("Video deleted")
		} else {
			fmt.Println("Video not found")
		}
	},
}

func init() {
	rootCmd.AddCommand(deleteCmd)
}
