/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"github.com/glebarez/sqlite"
	"gorm.io/gorm"
)

type Video struct {
	gorm.Model
	Name      string
	Url       string
	Available bool
	Ref       string
	DateAdded string
}

func setup() (db *gorm.DB) {
	db, err := gorm.Open(sqlite.Open("Videos.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	// Migrate the schema
	db.AutoMigrate(&Video{})
	return
}
