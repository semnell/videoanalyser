package cmd

import (
	"fmt"
	"strings"
	"testing"
)

func TestCheckIfVideoExists(t *testing.T) {
	out := RunCliCommand([]string{"add", "https://www.youtube.com/watch?v=jNQXAC9IVRw"})
	fmt.Print(out)
	if strings.Contains(out, "Video not available") {
		t.Errorf("This vid should be available")
	}
	out = RunCliCommand([]string{"add", "https://www.youtube.com/watch?v=1234567890"})
	if !strings.Contains(out, "Video not available") {
		t.Errorf("This vid should not be available")
	}
}
