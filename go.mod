module gitlab.com/semnell/videoanalyser

go 1.19

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/glebarez/sqlite v1.5.0
)

require (
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/glebarez/go-sqlite v1.19.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	modernc.org/libc v1.19.0 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/memory v1.4.0 // indirect
	modernc.org/sqlite v1.19.1 // indirect
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/fatih/color v1.13.0
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/manifoldco/promptui v0.9.0
	github.com/spf13/cobra v1.6.0
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
	gorm.io/gorm v1.24.0
)
