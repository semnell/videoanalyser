# videoAnalyser

A tool to analyse videos and collect them in a database.
The tool is able to detect if the video is still up and on some selected video providers it can also detect the title


# Example

```bash
videoAnalyser add https://www.youtube.com/watch?v=QH2-TGUlwu4
videoAnalyser delete https://www.youtube.com/watch?v=QH2-TGUlwu4
videoAnalyser list
videoAnalyser check
```